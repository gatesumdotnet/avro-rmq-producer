package webservices

type JobRequest struct {
	Name  string `json:"name"`
	JobId string `json:"jobId"`
}
