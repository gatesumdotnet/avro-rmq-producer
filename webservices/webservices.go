package webservices

import (
	"avro-rmq-producer/avro/jobrecord"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strings"
)

type WebServices struct {
	jobRecordStreamWriter jobrecord.SchemaParser
}

func New() *WebServices {
	return &WebServices{}
}

func (webServices *WebServices) Initialize() *mux.Router {
	fmt.Println("Initializing webservices.")
	router := mux.NewRouter()

	router.Path("/api/v1/job").
		Methods(http.MethodPost).
		HandlerFunc(webServices.HandleCreateJob)

	printRoutes(router)

	return router
}

func (webServices *WebServices) HandleCreateJob(writer http.ResponseWriter, request *http.Request) {
	decoder := json.NewDecoder(request.Body)
	jobRequest := &JobRequest{}
	_ = decoder.Decode(jobRequest)
	//TODO verify contents of request, send back error if problem.
	bytes, _ := jobrecord.Write(jobrecord.JobAvroRecord{
		Name:  jobRequest.Name,
		JobId: jobRequest.JobId,
	})
	_, _ = writer.Write(bytes)
	// TODO send to stream
}

func printRoutes(router *mux.Router) {
	_ = router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		path, err := route.GetPathTemplate()
		if err != nil {
			return err
		}
		methods, _ := route.GetMethods()
		fmt.Printf("[Registering Endpoint] path=%s methods=[%s]\n", path, strings.Join(methods, ", "))
		return nil
	})
}
