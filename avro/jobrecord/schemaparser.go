package jobrecord

import (
	"avro-rmq-producer/avro/schemamanager"
	"github.com/hamba/avro"
	"sync"
)

// Schema file name
const SchemaV1 = "jobrecord.avsc"

// Singleton definition of SchemaParser for JobRecord
var instance *SchemaParser

// sync.Once will only ever run once, this keeps our Singleton... single.
var once sync.Once

// Initialization function for SchemaParser
func NewSchemaParser() {
	schema := schemamanager.LoadSchema(SchemaV1)
	instance = &SchemaParser{
		schema: schema,
	}
}

// Package function that creates and accesses the singleton to read the avro data.
func Read(data []byte) (*JobAvroRecord, error) {
	// maybe create new schemaParser
	once.Do(NewSchemaParser)
	return instance.read(data)
}

// Package function that creates and accesses the singleton to write the avro data.
func Write(record JobAvroRecord) ([]byte, error) {
	// maybe create new schemaParser
	once.Do(NewSchemaParser)
	return instance.write(record)
}

// Type container for schema and functions related to job record
type SchemaParser struct {
	schema avro.Schema
}

// internal read function for Schema Parser
// writes the record to an avro byte array
func (parser *SchemaParser) read(data []byte) (*JobAvroRecord, error) {
	record := &JobAvroRecord{}
	err := avro.Unmarshal(parser.schema, data, record)
	return record, err
}

// internal write function for Schema Parser
// writes the record to an avro byte array
func (parser *SchemaParser) write(record JobAvroRecord) ([]byte, error) {
	return avro.Marshal(parser.schema, record)
}

