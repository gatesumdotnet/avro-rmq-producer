package jobrecord

// Avro record definition.
type JobAvroRecord struct {
	Name  string `avro:"name"`
	JobId string `avro:"jobId"`
}
