package schemamanager

import (
	"fmt"
	"github.com/gobuffalo/packr"
	"github.com/hamba/avro"
	"github.com/landoop/schema-registry"
	"sync"
)

// Singleton definition for SchemaManager.
var instance *SchemaManager
var once sync.Once

// Singleton accessor for SchemaManager.
func GetInstance() *SchemaManager {
	once.Do(func() {
		instance = newSchemaManager()
	})
	return instance
}

type SchemaManager struct {
	registryClient *schemaregistry.Client
	schemaBox      packr.Box
	schemas        map[string]avro.Schema
}

func newSchemaManager() *SchemaManager {
	client, _ := schemaregistry.NewClient("localhost")
	return &SchemaManager{
		registryClient: client,
		schemaBox:      packr.NewBox("../schemas"),
		schemas:        make(map[string]avro.Schema),
	}
}

func (schemaManager *SchemaManager) loadSchema(schemaName string) avro.Schema {
	codec := schemaManager.schemas[schemaName]
	if codec == nil {
		file, err := schemaManager.schemaBox.FindString(schemaName)
		if err != nil {
			fmt.Println(err)
		}
		codec, err = avro.Parse(file)
		if err != nil {
			fmt.Println(err)
		}
		schemaManager.schemas[schemaName] = codec
	}
	return codec
}

func LoadSchema(schemaName string) avro.Schema {
	return GetInstance().loadSchema(schemaName)
}
