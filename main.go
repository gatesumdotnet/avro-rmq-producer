package main

import (
	"avro-rmq-producer/webservices"
	"log"
	"net/http"
	"time"
)

func main() {
	//TODO init webservices
	services := webservices.New()
	router := services.Initialize()
	srv := &http.Server{
		Handler: router,
		Addr:    ":8080",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Running.")
	log.Fatal(srv.ListenAndServe())

	//TODO consume REST calls
	//TODO push calls onto Rabbit queue
}
