module avro-rmq-producer

go 1.12

require (
	github.com/gobuffalo/envy v1.7.1 // indirect
	github.com/gobuffalo/packr v1.30.1
	github.com/google/wire v0.3.0 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/hamba/avro v0.7.0
	github.com/landoop/schema-registry v0.0.0-20190327143759-50a5701c1891
	github.com/linkedin/goavro/v2 v2.9.6
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
